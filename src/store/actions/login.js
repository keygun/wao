import * as actionType from "./actionType";

export const  loginQuery = (criterias) => {
    return {
        type: actionType.loginData.LOGIN_QUERY,
        criterias: criterias 
    }
}

export const loginQueryStart = () => {
    return {
        type: actionType.loginData.LOGIN_QUERY_START
    }
}

export const loginQuerySuccess = (result) => {
    return {
        type: actionType.loginData.LOGIN_QUERY_SUCCESS,
        loginQueryResult: result
    }
}

export const loginQuerySuccessInvalid = (error) => {
    return {
        type: actionType.loginData.LOGIN_QUERY_SUCCESS_INVALID,
        loginQueryError: error
    }
}

export const loginQueryFail = () => {
    return {
        type: actionType.loginData.LOGIN_QUERY_FAIL
    }
}

export const loginClean  = (result) => {
    return {
        type: actionType.loginData.LOGIN_CLEAN,
        loginQueryResult: result,
        loginQueryError: result
    }
}

export const loginRefresh  = (criterias) => {
    return {
        type: actionType.loginData.LOGIN_REFRESH,
        criterias: criterias
    }
}