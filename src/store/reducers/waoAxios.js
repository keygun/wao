import * as actionType from '../actions/actionType';
import { updateObject} from '../../shared/utility';

const initialState = {
    processing: false,
    headerMapping: {}
}

const waoAxiosRequestStart = (state, action) => {
    return updateObject(state, {processing: true});
};

const waoAxiosRequestSuccess = (state, action) => {
    return updateObject(state, {processing: false});
};

const waoAxiosRequestFail = (state, action) => {
    return updateObject(state, {processing: false});
};

const waoAxiosLoadRequestDefaultHeaders = (state, action) => {
    const newHeaderMapping = updateObject(state.headerMapping, action.headerMapping);
    return updateObject(state, {headerMapping: newHeaderMapping});
};

const waoAxiosRequest = (state = initialState, action) => {
    switch (action.type) {
        case actionType.WAO_AXIOS_REQUEST_START:
            return waoAxiosRequestStart(state, action);
        case actionType.WAO_AXIOS_REQUEST_SUCCESS:
            return waoAxiosRequestSuccess(state, action);
        case actionType.WAO_AXIOS_REQUEST_FAIL:
            return waoAxiosRequestFail(state, action);
        case actionType.WAO_AXIOS_LOAD_REQUEST_DEFAULT_HEADERS:
            return waoAxiosLoadRequestDefaultHeaders(state, action);
        default:
            return state;
    }
}
export default waoAxiosRequest;