import * as actionTypes from '../actions/actionType';
import {takeEvery} from "redux-saga/effects";
import * as feelingSaga from './feeling';
import * as loginSaga from './login';

function * watchFeeling() {
    yield takeEvery(actionTypes.feelingData.FEELING_QUERY, feelingSaga.querySaga);
}

function * watchLogin() {
    yield takeEvery(actionTypes.loginData.LOGIN_QUERY, loginSaga.querySaga);
}

const applicationSagaContainer = [
    watchFeeling,
    watchLogin
];

export default applicationSagaContainer;