import moment from 'moment';
import {Modal} from 'antd';
import React, {Fragment} from 'react';
import jwt from 'jwt-simple';
import Cookies from 'universal-cookie';
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}

export const TCKN_REGEX = "^[0-9]{11}$";
export const TARIHI_REGEX = "^[0-9]{8}$";
export const SAYI_REGEX = "^[0-9]+$";
export const TEL_REGEX = "^[0-9]{10}$";
export const JAVA_MAX_INTEGER = 100000;
export const DATE_FORMAT = "DD/MM/YYYY";

export const clone = (data) => {
    if (!data) {
        return data;
    }
    const cloneData = JSON.parse(JSON.stringify(data));
    if (data instanceof moment) {
        return moment(cloneData);
    }
    return cloneData;
}

export const errorMessage = (error) => {
    const response = error.response;
    console.log('ERROR:', response);
    if (response && response.status === 401) {
        return;
    }
    let errContent = 'Sistemde sorun yaşandığı için işleminiz gerçekleştirilemedi.';
    if (response && response.data && response.data.hataVar) {
        errContent = response.data.hataKodu + " : " + response.data.hataAciklamasi;
        const sonucAciklamasiList = response.data.sonucAciklamasiList.map(item => <Fragment>{item}<br /></Fragment>);
        errContent = <Fragment>{errContent}<br />{sonucAciklamasiList}</Fragment>;
    }
    Modal.error({okText: 'Devam Et', title: 'Hatalı İşlem', content: errContent});
}

export const clearAllValue = (kriterler, isClone) => {
    if (isClone) {
        kriterler = clone(kriterler);
    }
    const keys = Object.keys(kriterler);
    for (const key of keys) {
        const val = kriterler[key];
        if (val !== null && typeof(val) === 'object') {
            clearAllValue(val, false);
        } else if (val === 'Hepsi') {
            kriterler[key] = null;
        }
    }
    return kriterler;
} 

export const decodeUiInitParams = (uiParamToken) => {
    if (uiParamToken !== undefined && uiParamToken !== 'undefined') {
        return jwt.decode(uiParamToken, Buffer.from('T1duNFFjTkVpRg==', 'base64').toString(), false, 'HS512')
    }
}

export const getRefreshTokenEndPoint = () => {
    return getInitParam('WAO_TEMPLATE_REFRESH_TOKEN_END_POINT')
}

export const getUIparamFromCookie = () => {
    return Cookies.get('UParam')
}

export const getInitParam = (param) => {
    const paramList = getInitParamList();
    if (paramList) {
        /*const mappedParam = UI_PARAM_MAPPING[param];
        return paramList[mappedParam];*/
    }
}

export const getInitParamList = () => {
    const cookieParam = getUIparamFromCookie();
    if (cookieParam) {
        return decodeUiInitParams(cookieParam)['initialUiParams'];
    }
}

export const encodeCommonUiParam = (param) =>  {
    return jwt.encode(param, getCommonUiParamsJwtKey());
}

export const getCommonUiParamsJwtKey = () => {
    Buffer.from(getInitParam('WAO_TEMPLATE_COMMON_UI_PARAMS_JWT_KEY'), 'base64').toString();
}