
export const dateDonustur = (response) => {
    return response.data.resultList.map((item, index) => ({
        ...item,
        key: index,
        onayListe: {
            ...item.onayListe,
            islemTarihi: item.onayListe.islemTarihi? new Date(item.onayListe.islemTarihi).toLocaleDateString("tr-TR"):item.onayListe.islemTarihi,
        }
    }));
}