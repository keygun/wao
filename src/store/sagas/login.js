import { put }  from "redux-saga/effects";
import { callServicePost } from "../../shared/axiosUtility";
import { errorMessage } from "../../shared/utility";
import * as actions from '../actions'

export function* querySaga(action)  {
    yield put(actions.loginQueryStart());
    try {
        //servis çağır
        const response = yield callServicePost("REACT_APP_RM_END_POINT_LOGIN_QUERY", action.criterias, true)
        yield put(actions.loginQuerySuccess(response.out));

    } catch (error) {
        console.log('Error', error.response);
        errorMessage(error);
        yield put(actions.loginQueryFail({error: error}));
    }

}