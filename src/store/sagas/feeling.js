import { put }  from "redux-saga/effects";
import { callServicePost } from "../../shared/axiosUtility";
import { errorMessage } from "../../shared/utility";
import * as actions from '../actions';

export function* querySaga(action)  {
    yield put(actions.feelingQueryStart());
    try {
        //servis çağır
        const response = yield callServicePost("REACT_APP_RM_END_POINT_QUERY", action.criterias, true)
        yield put(actions.feelingQuerySuccess(response.out));

    } catch (error) {
        console.log('Error', error.response);
        errorMessage(error);
        yield put(actions.feelingQueryFail({error: error}));
    }

}