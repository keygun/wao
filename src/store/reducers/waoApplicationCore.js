import * as actionType from '../actions/actionType';
import { updateObject} from '../../shared/utility';

const initialState = {
    appInitStarted: false,
    token: null,
    appCoreData: {},
    appInitError: null,
    logoutError: null
}

const waoApplicationInitStart = (state, action) => {
    return updateObject(state, {appInitStarted: true});
};

const waoApplicationInitSuccess = (state, action) => {
    return updateObject(state, {appCoreData: action.result, token: action.token});
};

const waoApplicationInitFail = (state, action) => {
    return updateObject(state, {appInitError: action.error});
};

const waoApplicationPageLoadSuccess = (state, action) => {
    return updateObject(state, {appCoreData: action.result, token: action.token});
};

const waoApplicationPageLoadFail = (state, action) => {
    return updateObject(state, {appInitError: action.error});
};

const waoApplicationLogoutFail = (state, action) => {
    return updateObject(state, {logoutError: action.error});
};

const waoApplicationLogoutSuccess = () => {
    return initialState;
};

const waoApplicationInit = (state = initialState, action) => {
    switch (action.type) {
        case actionType.WAO_APPLICATION_INIT_START:
            return waoApplicationInitStart(state, action);
        case actionType.WAO_APPLICATION_INIT_SUCCESS:
            return waoApplicationInitSuccess(state, action);
        case actionType.WAO_APPLICATION_INIT_FAIL:
            return waoApplicationInitFail(state, action);
        case actionType.WAO_APPLICATION_PAGE_LOAD_SUCCESS:
            return waoApplicationPageLoadSuccess(state, action);
        case actionType.WAO_APPLICATION_PAGE_LOAD_FAIL:
            return waoApplicationPageLoadFail(state, action);
        case actionType.WAO_APPLICATION_LOGOUT_FAIL:
            return waoApplicationLogoutFail(state, action);
        case actionType.WAO_APPLICATION_REFRESH_TOKEN_SUCCESS:
            return waoApplicationInitSuccess(state, action);
        case actionType.WAO_APPLICATION_REFRESH_TOKEN_FAIL:
            return waoApplicationInitFail(state, action);
        case actionType.WAO_APPLICATION_LOGOUT_SUCCESS:
            return waoApplicationLogoutSuccess();
        default:
            return state;
    }
}
export default waoApplicationInit;