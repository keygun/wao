import * as actionType from '../actions/actionType';
import * as reducerHelper from './reducerHelper'

const initialState = {
    criterias: {},
    queryResult: {},
    querySituation: false
}

const login = (state = initialState, action) => {
    switch (action.type) {
        case actionType.loginData.LOGIN_QUERY:
            return reducerHelper.query(state);
        case actionType.loginData.LOGIN_QUERY_START:
            return reducerHelper.queryStart(state);
        case actionType.loginData.LOGIN_QUERY_SUCCESS:
            return reducerHelper.querySuccess(state, action);
        case actionType.loginData.LOGIN_QUERY_FAIL:
            return reducerHelper.queryFail(state, action);
        case actionType.loginData.LOGIN_CLEAN:
            return reducerHelper.reducerClean(state, initialState, action);
        case actionType.loginData.LOGIN_REFRESH:
            return reducerHelper.criteriaRefresh(state, action);
        default:
            return state;
    }
}

export default login;