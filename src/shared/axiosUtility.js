import cbAxios, {create} from '../containers/Component/WAOAxios'
import { clearAllValue } from './utility';
export function * callServicePost(serviceKey, kriterler={}, isClearHepsi) {
    if (isClearHepsi) {
        kriterler = clearAllValue(kriterler, true);
    }
    console.log('callServicePost render geldim');

    const servicePath = process.env[serviceKey];
    const response = yield cbAxios.post(servicePath, kriterler);
    return response;
}
