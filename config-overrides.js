const {override, fixBabelImports, addWebpackPlugin}  = require('customize-cra');
const  CopyModulesPlugin = require("copy-modules-webpack-plugin");

module.exports = override(
    addWebpackPlugin(
        new CopyModulesPlugin({
            destination: 'webpack_modules'
        })
    ),
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true

    })

)