import FeelingList from '../containers/Feeling/List'
import LoginHandler from '../containers/Login/LoginHandler'
import SessionTimeout from '../containers/Logout/SessionTimeout'
import GeneralError from '../containers/Component/GeneralError'
import _404_ from '../containers/Component/NotFound404'

export const mapping = [
    {
        path: "/Feeling/List",
        exact: true,
        component: FeelingList
    }
]

export const commonMapping = [
    {
        path: "/login",
        exact: true,
        component: LoginHandler
    }, {
        path: "/logout",
        exact: true,
        component: SessionTimeout
    }, {
        path: "/error",
        exact: true,
        component: GeneralError
    }, {
        path: "/",
        component: _404_
    }
]