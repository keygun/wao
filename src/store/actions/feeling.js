import * as actionType from "./actionType";

export const  feelingQuery = (criterias) => {
    return {
        type: actionType.feelingData.FEELING_QUERY,
        criterias: criterias 
    }
}

export const feelingQueryStart = () => {
    return {
        type: actionType.feelingData.FEELING_QUERY_START
    }
}

export const feelingQuerySuccess = (result) => {
    return {
        type: actionType.feelingData.FEELING_QUERY_SUCCESS,
        feelingQueryResult: result
    }
}

export const feelingQuerySuccessInvalid = (error) => {
    return {
        type: actionType.feelingData.FEELING_QUERY_SUCCESS_INVALID,
        feelingQueryError: error
    }
}

export const feelingQueryFail = () => {
    return {
        type: actionType.feelingData.FEELING_QUERY_FAIL
    }
}

export const feelingClean  = (result) => {
    return {
        type: actionType.feelingData.FEELING_CLEAN,
        feelingQueryResult: result,
        feelingQueryError: result
    }
}

export const feelingRefresh  = (criterias) => {
    return {
        type: actionType.feelingData.FEELING_REFRESH,
        criterias: criterias
    }
}