import * as actionType from '../actions/actionType';
import * as reducerHelper from './reducerHelper'

const initialState = {
    criterias: {},
    queryResult: {},
    querySituation: false
}

const feeling = (state = initialState, action) => {
    switch (action.type) {
        case actionType.feelingData.FEELING_QUERY:
            return reducerHelper.query(state);
        case actionType.feelingData.FEELING_QUERY_START:
            return reducerHelper.queryStart(state);
        case actionType.feelingData.FEELING_QUERY_SUCCESS:
            return reducerHelper.querySuccess(state, action);
        case actionType.feelingData.FEELING_QUERY_FAIL:
            return reducerHelper.queryFail(state, action);
        case actionType.feelingData.FEELING_CLEAN:
            return reducerHelper.reducerClean(state, initialState, action);
        case actionType.feelingData.FEELING_REFRESH:
            return reducerHelper.criteriaRefresh(state, action);
        default:
            return state;
    }
}

export default feeling;