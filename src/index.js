import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import en_GB from 'antd/lib/locale-provider/en_GB'
import { ConfigProvider } from 'antd';
import applicationSagaContainer from './store/sagas';
import applicationRootReducer from './store/reducers';
import { BrowserRouter } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import { Provider } from 'react-redux';
import { createWaoStore } from './store';

const store  = createWaoStore(applicationRootReducer, applicationSagaContainer)
ReactDOM.hydrate(
  <ConfigProvider locale={en_GB}>
    <Provider store={store}>
      <CookiesProvider>
        <BrowserRouter basename={process.env.REACT_APP_APPLICATION_CONTEXT_PATH}>
          <App />        
        </BrowserRouter>
      </CookiesProvider>
    </Provider>
  </ConfigProvider>,
  //<React.StrictMode>
  //  <App />
  //</React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
