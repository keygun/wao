import React, {Component} from 'react';
import 'antd/dist/antd.css'
import {Layout} from 'antd';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import {withRouter} from 'react-router-dom';

class WAOAuthenticatedTemplate extends Component {
    setDocumentTitle() {
        if (this.props.application && this.props.application.name){
            document.title = this.props.application.name;
        }
    }

    cookieControlHandler = () => {

    }
    
}


const mapStateToProps = state => {
    const appCoreData = state.waoFrameworkRootReducer.waoApplicationInit.appCoreData;
    const token = state.waoFrameworkRootReducer.waoApplicationInit;
    return {
        user: appCoreData.user,
        application: appCoreData.application,
        userMenuTree: appCoreData.userMenuTree,
        patchMenuMatch: appCoreData.patchMenuMatch,
        isAuthenticated:  token !== null && token !== undefined
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(actions.waoApplicationLogout()),
        onRefreshToken: () => dispatch(actions.waoApplicationRefreshToken())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (WAOAuthenticatedTemplate));