import * as actionType from './actionType'

export const waoApplicationInit = (props) => {
    return {
        type: actionType.WAO_APPLICATION_INIT
    };
};

export const waoApplicationInitStart = () => {
    return {
        type: actionType.WAO_APPLICATION_INIT_START
    };
};

export const waoApplicationInitSuccess = (result) => {
    return {
        type: actionType.WAO_APPLICATION_INIT_SUCCESS,
        result: result
    }
}

export const waoApplicationInitFail = (error) => {
    return {
        type: actionType.WAO_APPLICATION_INIT_FAIL,
        error: error
    }
}

export const waoApplicationPageLoad = (props) => {
    return {
        type: actionType.WAO_APPLICATION_PAGE_LOAD
    };
};

export const waoApplicationPageLoadStart = () => {
    return {
        type: actionType.WAO_APPLICATION_PAGE_LOAD_START
    };
};

export const waoApplicationPageLoadSuccess = (result, token) => {
    return {
        type: actionType.WAO_APPLICATION_PAGE_LOAD_SUCCESS,
        result: result,
        token: token
    }
}

export const waoApplicationPageLoadFail = (error) => {
    return {
        type: actionType.WAO_APPLICATION_PAGE_LOAD_FAIL,
        error: error
    }
}

export const waoApplicationLogout = (props) => {
    return {
        type: actionType.WAO_APPLICATION_LOGOUT
    };
};

export const waoApplicationLogoutStart = () => {
    return {
        type: actionType.WAO_APPLICATION_LOGOUT_START
    };
};

export const waoApplicationLogoutSuccess = (result) => {
    return {
        type: actionType.WAO_APPLICATION_LOGOUT_SUCCESS,
        result: result
    }
}

export const waoApplicationLogoutFail = (error) => {
    return {
        type: actionType.WAO_APPLICATION_LOGOUT_FAIL,
        error: error
    }
}

export const waoApplicationRefreshToken = (token) => {
    return {
        type: actionType.WAO_APPLICATION_REFRESH_TOKEN,
        token: token
    };
};

export const waoApplicationRefreshTokenStart = () => {
    return {
        type: actionType.WAO_APPLICATION_REFRESH_TOKEN_START
    };
};

export const waoApplicationRefreshTokenSuccess = (result) => {
    return {
        type: actionType.WAO_APPLICATION_REFRESH_TOKEN_SUCCESS,
        result: result
    }
}

export const waoApplicationRefreshTokenFail = (error) => {
    return {
        type: actionType.WAO_APPLICATION_REFRESH_TOKEN_FAIL,
        error: error
    }
}