import axios from 'axios';
import Cookies from 'universal-cookie';
import {getWAOStore} from '../../store';
import * as actionTypes from '../../store/actions';
import * as utility from '../../shared/utility';

const cookies = new Cookies();

export const create = (config) => {
    const instance = axios.create(config);
    setRequestTimeOut(config);
    addWAOInterceptors(instance);
    return instance;
}

const setRequestTimeOut = (config) => {
    if (!config.timeout || config.timeout === 0) {
        console.log('setRequestTimeOut render geldim');
        config['timeout'] = parseInt(process.env.REACT_APP_WAO_TEMPLATE_DEFAULT_REQUEST_TIMEOUT_SECOND,10) * 1000;
    }
}

const addDefaultHeaders = (headers) => {
    try {
        const state = getWAOStore().getState();
        const axiosState = state.waoRootReducer.waoAxiosRequest;
        const currPath = window.location.pathname;
        let mapping = axiosState.headerMapping[currPath];
        if (!mapping || !mapping["ui-req-menu-name"]) {
            mapping = loadRequestMenuUrlMappings(currPath);
        }
        try {

            if (mapping) {
                Object.keys(mapping).forEach(key => {
                    headers[key] = mapping[key]
                })
            }
        } catch (error) {
            console.log(error)
        }

    } catch (error) {
        console.log(error)
    }
}

const loadRequestMenuUrlMappings = (path) => {
    console.log('loadRequestMenuUrlMappings render geldim');

    let headerMapping = {};
    try {
        const state = getWAOStore().getState();
        const coreState = state.waoFrameworkRootReducer.waoApplicationInt.appCoreData;
        if (coreState && coreState.userMenuTree) {
            let subMenus = [];
            coreState.userMenuTree.forEach(menu => {
                subMenus = subMenus.concat(menu.subMenus);
            });
            let filteredMenus = subMenus.filter(subMenu => {
                const sMenu = subMenu.menuUrl.startsWith("/")?subMenu.menuUrl: "/" + subMenu.menuUrl;
                const sMenuUrl = process.env.REACT_APP_APPLICATION_CONTEXT_PATH + sMenu;
                return path  === sMenuUrl
            })
            if (filteredMenus && filteredMenus.length >0) {
                const currMenu = filteredMenus[0];
                try {
                    headerMapping[path] = {
                        "req-go-id": utility.encodeCommonUiParam(currMenu.goId.toString()),
                        "req-menu-name": utility.encodeCommonUiParam(currMenu.aciklama)
                    }
                } catch (error) {
                    console.log(error)
                }
            }
        }
        const encrypPath  = utility.encodeCommonUiParam(path);
        if (!headerMapping[path]){
            headerMapping[path] = {}
        }
        headerMapping[path]['req-path'] = encrypPath;
        getWAOStore().dispatch(actionTypes.waoAxiosLoadRequestDefaultHeaders(headerMapping));

    } catch (error) {
        console.log(error)
    }
    return headerMapping[path];
}

const addWAOInterceptors = (instance) => {
    addWAORequestInterceptor(instance);
    addWAOResponseInterceptor(instance);
}

const shouldTokenBeRefreshed = (refreshFlag, cookieToken, config) => {
    return !refreshFlag && cookieToken && !isRefreshTokenRequest(config);
}

const isRefreshTokenRequest = (config) => {
    let forReturn = false;
    const refreshTokenEndPoint = utility.getRefreshTokenEndPoint();
    forReturn = config.url === refreshTokenEndPoint;
    if (isApiGwDevelopmentRouter()) {
        forReturn = config.url === produceRouteUrl(refreshTokenEndPoint);
    }
    return forReturn;
}

const produceRouteUrl = (url) => {
    return process.ev.REACT_APP_WAO_TEMPLATE_API_GW_LOCAL_DEVELPOMENT_ROUTER_END_POINT.concat('?ip=').concat(encodeURIComponent(url));
}

const isApiGwDevelopmentRouter = () => {
    console.log('isApiGwDevelopmentRouter render geldim');
    return process.env.NODE_ENV === "development" && process.env.REAT_APP_WAO_TEMPLATE_USE_API_GW_DEV_ROUTER === "true"
}

const transformUrlforLocal = (config) => {
    if (isApiGwDevelopmentRouter() && config.url && config.url.toString().indexOf('?ip=') < 0) {
        const orj = config.url;
        config.url = produceRouteUrl(config.url);
        console.log('url <' + orj + '> transformed to -> <' + config.url + '>');
    }
}

const requestTransformer = (data, headers, config) => {
    transformUrlforLocal(config);
    const cookieToken = cookies.get('token');
    const refreshFlag = cookies.get('refreshFlag');
    if (shouldTokenBeRefreshed(refreshFlag, cookieToken, config)) {
        getWAOStore().dispatch(actionTypes.waoApplicationRefreshToken());
    }
    const token = cookieToken ? cookieToken: headers['Athorization'];
    if (token) {
        headers['Athorization'] = token;
        headers = addDefaultHeaders(headers);
    }
    headers.post['Content-Type'] = 'application/json;charset=UTF-8';
    headers.put['Content-Type'] = 'application/json;charset=UTF-8';
    if (data instanceof FormData) {
        headers.post['Content-Type'] = 'multipart/form-data';
        headers.put['Content-Type'] = 'multipart/form-data';
        return data;
    }
    return JSON.stringify(data);

}

const addWAORequestInterceptor = (instance) => {
    instance.interceptors.request.use((config)=> {
        getWAOStore().dispatch(actionTypes.waoAxiosRequestStart());
        config.transformRequest = [
            (data, headers) => requestTransformer(data, headers, config)
        ];
        return config;
    }, error => {
        getWAOStore().dispatch(actionTypes.waoAxiosRequestFail());
        return Promise.reject(error);
    })
}


const addWAOResponseInterceptor = (instance) => {
    instance.interceptors.response.use((response)=> {
        getWAOStore().dispatch(actionTypes.waoAxiosRequestSuccess());
        return response;
    }, error => {
        getWAOStore().dispatch(actionTypes.waoAxiosRequestFail());
        if (error.response &&  (error.response.status === 401 || error.response.status === 451) && !isLifeCycleService(error.config)) {
            console.log('addWAOResponseInterceptor render geldim');
            window.location.href=process.env.REACT_APP_APPLICATION_CONTEXT_PATH +  "/logout"
        }
        return Promise.reject(error);
    })
}

const isLifeCycleService = (config) => {
    const paramList = utility.getInitParamList();
    if (!paramList) {
        return false;
    }
    let url = config.url.toString();
    /*for (let param of utility.LIFE_CYCLE_SERVICE_NAME_LIST ) {
        const mappedParam = utility.UI_PARAM_MAPPING[param];
        let lifeCycleUrl=produceRouterUrl(paramList[mappedParam]);
        if (isApiGwDevelopmentRouter()) {
            lifeCycleUrl = produceRouteUrl(paramList[mappedParam]);
        }
        if (url===lifeCycleUrl) {
            return true;
        }
    } */
    return false;
}

const instance = axios.create({ timeout: parseInt(process.env.REACT_APP_WAO_TEMPLATE_DEFAULT_REQUEST_TIMEOUT_SECOND, 10)*1000});
instance.defaults.headers.post['Content-Type']  = 'application/json;charset=UTF-8,multipart/form-data' 
instance.defaults.headers.put['Content-Type'] = 'application/json;charset=UTF-8,multipart/form-data'
addWAOInterceptors(instance);

export default instance;