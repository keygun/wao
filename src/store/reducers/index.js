import {combineReducers} from 'redux';
import feeling from './feeling';
import login from './login';
import waoAxiosRequest from './waoAxios'
import waoApplicationInit from './waoApplicationCore'

const applicationRootReducer = combineReducers({
    feeling,
    login,
    waoAxiosRequest,
    waoApplicationInit,

});

export default applicationRootReducer