import * as actionType from './actionType';


export const waoAxiosRequestStart = () => {
    return {
        type: actionType.WAO_AXIOS_REQUEST_START
    };
};

export const waoAxiosRequestSuccess = () => {
    return {
        type: actionType.WAO_AXIOS_REQUEST_SUCCESS
    }
}

export const waoAxiosRequestFail = () => {
    return {
        type: actionType.WAO_AXIOS_REQUEST_FAIL
    }
} 

export const waoAxiosLoadRequestDefaultHeaders = (headerMapping) => {
    return {
        type: actionType.WAO_AXIOS_LOAD_REQUEST_DEFAULT_HEADERS,
        headerMapping: headerMapping
    };
}


