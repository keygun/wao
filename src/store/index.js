import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import  createSagaMiddleware from "redux-saga"
import waoFrameworkRootReducer from "./reducers";
import waoFrameworkSagaContainer from "./sagas";

let waoStore = null;

export const getWAOStore = () =>  {
    if (waoStore === null) {
        throw new Error('waoSotre is empty')
    } else {
        return waoStore;
    }
}

export const createWaoStore = (applicationRootReducer, applicationSagaContainer) => {
    const waoRootReducer = combineReducers({
        applicationRootReducer, waoFrameworkRootReducer
    });
    const sagaMiddleware = createSagaMiddleware();
    waoStore = createStore(waoRootReducer, compose(applyMiddleware(sagaMiddleware)));
    applicationSagaContainer.map(saga=>sagaMiddleware.run(saga));
    waoFrameworkSagaContainer.map(saga=>sagaMiddleware.run(saga));
    return waoStore;

}