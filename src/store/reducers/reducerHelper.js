import { clone, updateObject} from '../../shared/utility';

const success = (state, action, resultFieldName, statusFieldName) => {
    return updateObject(state, {
        [resultFieldName]: action.result,
        [statusFieldName]: true
    });
};

export const query = (state) => {
    return updateObject(state);
}

export const queryStart = (state, statusFieldName = "querySituation" ) => {
    return updateObject(state, {
        [statusFieldName]: false
    });
};

export const querySuccess = (state, action, resultFieldName= "queryResult",statusFieldName = "querySituation" ) => {
    return success(state, action, resultFieldName, statusFieldName );
};

export const queryFail = (state, action) => {
    return updateObject(state, {error: action.error});
};

export const reducerClean = (state, initialState, action) => {
    const cleanFields = action.cleanFields;
    let toReturn;
    if (!cleanFields || cleanFields.length === 0) {
        toReturn = clone(initialState);
    } else {
        toReturn = updateObject(state);
        for (const field of cleanFields) {
            toReturn[field] = null;
        }
    }
    return toReturn;
};

export const criteriaRefresh = (state, action) => {
    return updateObject(state, {
        criterias: action.criterias
    });
}