import React, { Component } from "react";
import { connect } from "react-redux";
import WAOTemplate from "./containers/Component/WAOTemplate";
import Routes from "./routes/Routes";
import {withRouter} from 'react-router-dom';

class App extends Component {
  render() {
    const templatedPage = 
    <WAOTemplate>
        <Routes />
    </WAOTemplate>
    return (templatedPage);
  }
}
export default withRouter(connect()(App));