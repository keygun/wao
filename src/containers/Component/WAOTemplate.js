import React, {Component} from 'react';
import 'antd/dist/antd.css'
import {connect} from 'react-redux'
import * as actions from '../../store/actions'
import TemplateLoadingModel from './WAOAuthenticatedTemplate'
import WAOAuthenticatedTemplate from './WAOAuthenticatedTemplate';

class WAOTemplate extends Component {

    componentDidMount() {
        this.props.onPageLoad();
    }

    render () {
        console.log("Girdiiim")
        let toRender = null;
        if (this.props.isAuthenticated) {
            toRender = <WAOAuthenticatedTemplate>{this.props.children}</WAOAuthenticatedTemplate>
        } else if (!this.props.processing) {
            toRender = this.props.children;
        }
        toRender =<div id="div_waomaintemplate">
            {toRender}
            <TemplateLoadingModel visible={this.props.processing} />
        </div>
        return toRender;
    }
}
const mapStateToProps = state => {
    const token = state.waoFrameworkRootReducer.waoApplicationInit.token;
    return {
        processing: state.waoFrameworkRootReducer.waoAxiosRequest.processing,
        isAuthenticated: token!==null && token !== undefined 
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPageLoad: () => dispatch(actions.waoApplicationPageLoad())
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (WAOTemplate);