import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import {mapping, commonMapping} from './mapping';
import { connect } from 'react-redux';
import MainPage from '../containers/Component/MainPage'

class Routes extends React.Component {

    render() {

        console.log('roue render geldim', process.env.REACT_APP_APPLICATION_WELCOME_PAGE);
        let mappedRoutes = [];
        

        const mainPage = {
            path: '/main_page',
            exact: true,
            componenet: MainPage
        };

        console.log('roue render geldim', this.props.isAuthenticated);
        if (this.props.isAuthenticated && this.props.userMenuTree) {
            const menuUrlArr = [];
            this.props.userMenuTree.forEach(element => {
               return (element.subMenus.forEach((subMenu) => (menuUrlArr.push(subMenu.menuUrl))))
            });

            mappedRoutes = mappedRoutes.concat(
                mapping
                .filter((route) => menuUrlArr.includes(route.path))
                .map((route) => <Route key={route.path} {...route}></Route>)
            )
        }
        mappedRoutes = mappedRoutes.concat( <Route key={mainPage.path} {...mainPage}></Route>)
       // mappedRoutes = mappedRoutes.concat(commonMapping.map((route) => {
       //     return <Route key={route.path} {...route}></Route>
        //}));
        return <Switch location={this.props.location}>
            {mappedRoutes}
        </Switch>
    }
}

const mapStateToProps = state => {
    console.log('roue geldim');
    const appCoreData = state.waoFrameworkRootReducer.waoApplicationInit.appCoreData;
    const token = state.waoFrameworkRootReducer.waoApplicationInit.token;
    return {
        pathMenuMatch: appCoreData.pathMenuMatch,
        userMenuTree: appCoreData.userMenuTree,
        isAuthenticated: token!== null   && token !== undefined
    };
}

export default withRouter(connect(mapStateToProps)(Routes));